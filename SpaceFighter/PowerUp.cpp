#include "PowerUp.h"
#include "Level.h"

PowerUp::PowerUp()
{
	SetCollisionRadius(20);
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetScoreAddonPoints(20);
}

void PowerUp::Update(const GameTime* pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		TranslatePosition(0, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	GameObject::Update(pGameTime);
}

void PowerUp::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter());
	}
}

void PowerUp::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;
	m_hitPoints = 1;
}

void PowerUp::Hit(const float damage)
{
	m_hitPoints -= damage;

	if (m_hitPoints <= 0)
	{
		SetScore(m_scorePoints); // adds to score when ship is hit
		//std::cout << "\n" << ToString() << " | Points Added to Score: " << m_scorePoints << "\n" << "Overall Score: " << GameObject::GetScore() << "\n"; // for debugging purposes

		Deactivate();
	}
}
