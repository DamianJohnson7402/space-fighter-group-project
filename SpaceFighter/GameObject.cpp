#include "GameObject.h"
#include "Level.h"
#include <time.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
/*#include <ctime>
#include <stdlib.h>*/

uint32_t GameObject::s_count = 0;
Level* GameObject::s_pCurrentLevel = nullptr;
int GameObject::m_sScore = 0; // sets the score to zero
std::string m_filepath = "..\\SpaceFighter\\Content\\Score.txt";
std::string GameObject::m_isSaved = "";

struct tm newtime;
__time32_t aclock;

GameObject::GameObject()
{
	m_index = s_count;
	s_count++;

	m_isActive = false;
	m_collisionRadius = 0;
}

void GameObject::Update(const GameTime* pGameTime)
{
	if (IsActive() && s_pCurrentLevel)
	{
		s_pCurrentLevel->UpdateSectorPosition(this);
	}
}

Vector2 GameObject::GetHalfDimensions() const
{
	return Vector2(m_collisionRadius, m_collisionRadius);
}

void GameObject::SetPosition(const float x, const float y)
{
	m_previousPosition = m_position;
	m_position.Set(x, y);
}

void GameObject::SetPosition(const Vector2& position)
{
	SetPosition(position.X, position.Y);
}

void GameObject::TranslatePosition(const float x, const float y)
{
	SetPosition(m_position.X + x, m_position.Y + y);
}

void GameObject::TranslatePosition(const Vector2& offset)
{
	TranslatePosition(offset.X, offset.Y);
}

bool GameObject::IsOnScreen() const
{
	if (m_position.Y - GetHalfDimensions().Y >= Game::GetScreenHeight()) return false;
	if (m_position.Y + GetHalfDimensions().Y <= 0) return false;
	if (m_position.X - GetHalfDimensions().X >= Game::GetScreenWidth()) return false;
	if (m_position.X + GetHalfDimensions().X <= 0) return false;

	return true;
}

void GameObject::Deactivate()
{
	m_isActive = false;
}

void GameObject::Wait(int seconds)
{
	clock_t endwait;
	endwait = clock() + seconds * CLOCKS_PER_SEC;
	while (clock() < endwait) {}
}

void GameObject::SaveScore()
{
	std::ofstream ofs;
	ofs.open(m_filepath, std::ios::app);

	std::string old;
	if (ofs)
	{
		char buffer[32];
		errno_t errNum;
		_time32(&aclock);
		_localtime32_s(&newtime, &aclock);
		errNum = asctime_s(buffer, 32, &newtime);

		ofs << std::to_string(m_sScore) << " --- " << buffer << "\n";
		m_isSaved = "\nYour score has been saved!";
	}

	ofs.close();
}
/*
void GameObject::AddNewObjectToScreen(GameObject *pGameObject)
{
	s_pCurrentLevel->AddGameObject(pGameObject);
}

void GameObject::SetScore(int scoreAddon)
{
	m_score += scoreAddon;
}

int GameObject::GetScore()
{
	return m_score;
}*/