#pragma once
#include "GameObject.h"
//#include "Ship.h"

class Score : public GameObject
{
public:
	Score() { }
	virtual ~Score() { }

	virtual void Draw(SpriteBatch* pSpriteBatch);

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void Update(const GameTime* pGameTime);

	//virtual void SetScore(const int scoreAddon);// { m_score += scoreAddon; }

	//virtual int GetScore();// { return m_score; }

	virtual std::string ToString() const { return "Score"; }

	virtual CollisionType GetCollisionType() const { return (CollisionType::NONE | CollisionType::NONE); }

private:
	//int m_score;

	std::string m_text;

	bool m_isDisplayed;

	Font* m_pFont;

	Color m_color;

	float m_alpha;

	Vector2 m_position;

	Vector2 m_textOffset;

	TextAlign m_textAlign;
};

